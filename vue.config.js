const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
module.exports = {
    devServer: {
        disableHostCheck: false,
        host: "0.0.0.0",
        port: 20206,
        https: false,
        hotOnly: false,
        proxy: {
            "/api": {
                target: "http://10.152.153.90",
                // target: process.env.VUE_APP_LRP_API_test,
                changeOrigin: true
            },
        }
    },
    productionSourceMap: false,
    publicPath: "./",
    // css: {
    //     loaderOptions: {
    //         scss: {
    //             prependData: `@import "src/styles/global.scss";`
    //         }
    //     }
    // },
    // configureWebpack: config => {
    //     config.plugins.push(
    //         new CopyWebpackPlugin([{
    //             from: path.join(__dirname, "src/static"),
    //             to: path.join(__dirname, "dist/static"),
    //             ignore: [""]
    //         }])
    //     );
    // }
};