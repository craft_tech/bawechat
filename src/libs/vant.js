import {
    Loading,
    Cell,
    CellGroup,
    Form,
    Field,
    Button,
    ActionSheet,
    Toast,
    Calendar,
    Area,
    Popup
} from "vant";

import "vant/lib/loading/style";
import "vant/lib/cell/style";
import "vant/lib/cell-group/style";
import "vant/lib/form/style";
import "vant/lib/field/style";
import "vant/lib/button/style";
import "vant/lib/action-sheet/style";
import "vant/lib/toast/style";
import "vant/lib/calendar/style";
import "vant/lib/area/style";
import "vant/lib/popup/style";

const vant = {
    install(Vue) {
        Vue.use(Loading);
        Vue.use(Cell);
        Vue.use(CellGroup);
        Vue.use(Form);
        Vue.use(Field);
        Vue.use(Button);
        Vue.use(ActionSheet);
        Vue.use(Toast);
        Vue.use(Calendar);
        Vue.use(Area);
        Vue.use(Popup);
    }
};
export default vant;