// 判断访问终端
export const browser = {
    versions: function () {
        const u = navigator.userAgent,
            app = navigator.appVersion;
        return {
            trident: u.indexOf('Trident') > -1, // IE内核
            presto: u.indexOf('Presto') > -1, // opera内核
            webKit: u.indexOf('AppleWebKit') > -1, // 苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') === -1, // 火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), // 是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), // ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, // android终端
            iPhone: u.indexOf('iPhone') > -1, // 是否为iPhone或者QQHD浏览器
            iPhoneX: /iphone/gi.test(u) && screen.height >= 812, // iphoneX及后
            iPad: u.indexOf('iPad') > -1, // 是否iPad
            webApp: u.indexOf('Safari') === -1, // 是否web应该程序，没有头部与底部
            weixin: u.indexOf('MicroMessenger') > -1, // 是否微信 （2015-01-22新增）
            qq: u.match(/\sQQ/i) === " qq" // 是否QQ
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
};
export class checkList {
    constructor() {
        // constructor(phoneNum, codeNum) {
        // this.phoneNum = phoneNum;
        // this.codeNum = codeNum;
    }
    checkName(name, {
        empty,
        error
    } = callbackObj) {
        var reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
        if (name == '') {
            // alert('请输入姓名')
            empty && empty();
            return;
        }
        if (!reg.test(name)) {
            // alert('名字格式不正确')
            error && error();
            return false;
        } else {
            return true;
        }
    }
    checkPhoneNum(phoneNum, {
        empty,
        error
    } = callbackObj) {
        if (phoneNum == '') {
            // alert('请输入手机号');
            empty && empty();
            return;
        }
        if (!/^1[0-9][0-9]\d{8}$/.test(phoneNum)) {
            // alert('请输入正确的手机号码');
            error && error();
            return false;
        } else {
            return true;
        }
    }
    checkNumNotNull(num, alerTxt) {
        if (num == '' || num == 0) {
            alert(alerTxt || '数量不能为空');
            return false;
        } else {
            return true;
        }
    }
    checkCodeNum(codeNum, maxLen = 6, alerTxt) {
        if (codeNum == '' || codeNum.length != maxLen) {
            alert(alerTxt || '请输入验证码');
            return false;
        } else {
            return true;
        }
    }

    //省市联动判断是否取值obj={'选择省份':province}
    checkAppointmentCounter(obj) {
        for (let item in obj) {
            // console.log(item, obj[item])
            if (item == '' || item == obj[item]) {
                alert('请' + obj[item]);
                return false;
            }
        }

        return true;
    }
    checkPrivacy(status) {
        if (!status) {
            alert('请同意隐私条款');
            return false;
        } else {
            return true;
        }
    }
}

// 验证码倒计时
export const timeCountDown = function () {
    let smsCodeData = {
        changeSmsCode: "获取验证码",
        smsCodenowTime: 60,
        smsCodemaxTime: 60,
        smsCodetTime: "" // 获取的时间
    };
    let st = "";

    let _refreshSmsCodeData = function (_this) {
        clearTimeout(st);
        smsCodeData = {
            changeSmsCode: "获取验证码",
            smsCodenowTime: 60,
            smsCodemaxTime: 60,
            smsCodetTime: ""
        };
        _this.isGetSmsCode = false;
    };

    let _startChangeTime = function (_this, callback) {
        if (smsCodeData.smsCodetTime == null) {
            smsCodeData.smsCodetTime = new Date().getTime();
        } else {
            let t2 = new Date().getTime();
            // console.log(t2 - smsCodeData.smsCodetTime);
            if (
                t2 - smsCodeData.smsCodetTime <
                smsCodeData.smsCodemaxTime * 1000
            ) {
                smsCodeData.smsCodetTime = t2;
                return false;
            } else {
                smsCodeData.smsCodetTime = t2;
            }
        }
        callback && typeof callback === "function" && callback();
        _changeTimeNext(_this);
    };

    let _changeTimeNext = function (_this) {
        let changeSmsCode = "smsCodeData.changeSmsCode";
        if (smsCodeData.smsCodenowTime == 0) {
            smsCodeData.changeSmsCode = "获取验证码";
            smsCodeData.smsCodenowTime = smsCodeData.smsCodemaxTime;
            smsCodeData.smsCodetTime = "";

            _this.isGetSmsCode = false;
            _this.getSmsCodeTime = smsCodeData.smsCodenowTime + "s";

            clearTimeout(st);
        } else {
            smsCodeData.changeSmsCode = smsCodeData.smsCodenowTime;

            _this.getSmsCodeTime = smsCodeData.smsCodenowTime + "s";

            smsCodeData.smsCodenowTime--;

            st = setTimeout(function () {
                _changeTimeNext(_this);
            }, 1000);
        }
    };
    return {
        startChangeTime: function (_this, callback, errCallback) {

            if (_this.isGetSmsCode) {
                console.log("已经获取了");
                errCallback && errCallback()
                return false;
            } else {
                console.log("开始获取");
                _startChangeTime(_this, callback);
                _this.isGetSmsCode = true;
                return true;
            }
        },
        refreshChangeTime: function (_this, callback) {
            console.log("重新计时");
            _refreshSmsCodeData(_this);
        }
    };
}

export function getQueryParameter(name) {
    // location.search
    let url = decodeURIComponent(location.href);
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    // return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    return results == null ? "" : decodeURIComponent(results[1]);
}

// 2个时间戳的差
export function getChangeTime(dateBegin, dateEnd) {
    let dateDiff = dateEnd - dateBegin; //时间差的毫秒数
    let dayDiff = Math.floor(dateDiff / (24 * 3600 * 1000)); //计算出相差天数
    let leave1 = dateDiff % (24 * 3600 * 1000) //计算天数后剩余的毫秒数
    let hours = Math.floor(leave1 / (3600 * 1000)) //计算出小时数
    //计算相差分钟数
    let leave2 = leave1 % (3600 * 1000) //计算小时数后剩余的毫秒数
    let minutes = Math.floor(leave2 / (60 * 1000)) //计算相差分钟数
    //计算相差秒数
    let leave3 = leave2 % (60 * 1000) //计算分钟数后剩余的毫秒数
    let seconds = Math.round(leave3 / 1000)
    console.log(" 相差 " + dayDiff + "天 " + hours + "小时 " + minutes + "分钟 " + seconds + "秒")
    return {
        day: dayDiff,
        hours,
        minutes,
        seconds
    }
}

// 获取当前月的最大天数
export function getMonthMaxDate() {
    var curDate = new Date();
    var curMonth = curDate.getMonth();
    curDate.setMonth(curMonth + 1);
    curDate.setDate(0);
    return curDate.getDate();
}

export default {
    browser,
    checkList,
    timeCountDown
};