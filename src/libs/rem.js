function setRem() {
    let htmlWidth =
        (document.documentElement).clientWidth ||
        document.body.clientWidth;
    let htmlDom = document.getElementsByTagName("html")[0];
    let fontSize = ((htmlWidth / 20) * 8) / 3;
    htmlDom.style.fontSize = fontSize <= 100 ? fontSize + "px" : "100px";
}
setRem();
window.onresize = function () {
    setRem();
};
if (
    typeof window.WeixinJSBridge == "object" &&
    typeof window.WeixinJSBridge.invoke == "function"
) {
    handleFontSize();
} else {
    if (document.addEventListener) {
        document.addEventListener("WeixinJSBridgeReady", handleFontSize, false);
    } else if (document.attachEvent) {
        document.attachEvent("WeixinJSBridgeReady", handleFontSize);
        document.attachEvent("onWeixinJSBridgeReady", handleFontSize);
    }
}

function handleFontSize() {
    // 设置网页字体为默认大小
    window.WeixinJSBridge.invoke("setFontSizeCallback", {
        fontSize: 0
    });
    // 重写设置网页字体大小的事件
    window.WeixinJSBridge.on("menu:setfont", function () {
        window.WeixinJSBridge.invoke("setFontSizeCallback", {
            fontSize: 0
        });
    });
}