import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/result',
    name: 'result',
    component: () => import('../views/result/result.vue')
  },
  {
    path: '/ba',
    name: 'ba',
    component: () => import('../views/BA.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router