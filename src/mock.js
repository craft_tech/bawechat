import Mock from "mockjs";

Mock.mock(/\/api\/Server\/GetInfo/, "post", (req) => {
    return {"success":true,"msg":"","data":{"success":"true","msg":"成功","data":{"openid":"oPV4Ht1VfQ23we1xI9odBXxSIt-s","nickname":"Leo","sex":1,"province":"","city":"","country":"直布罗陀","headimgurl":"http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLdRNTRTUiaoqtUG2xpUicgLUqytLukRb0CRhe8CfOVGUS7M70e4hRcXia0Vj7UWN8r0G3xDXhIibcsFw/132","privilege":[],"unionid":"oa3x4xGQ30L1UXX2vtrYcO8b-K20"},"png":"","token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Im9QVjRIdDFWZlEyM3dlMXhJOW9kQlh4U0l0LXMiLCJyb2xlIjoiMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDAtMDAwMDAwMDAwMDAwIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9leHBpcmF0aW9uIjoiNi8xNy8yMDIwIDI6NTg6MTcgUE0iLCJuYmYiOjE1OTIzOTE0OTcsImV4cCI6MTU5MjQwNTg5NywiaWF0IjoxNTkyMzkxNDk3LCJpc3MiOiJnaW9yZ2lvYXJtYW5pYmVhdXR5LmNvbSIsImF1ZCI6Imdpb3JnaW9hcm1hbmliZWF1dHkuY29tIn0.p6FrbXNbw1DgU9lGlE7YSKgxXwbqI938AibUA6PWsFI"}}
});

Mock.mock(/\/api\/Server\/GetJsSdkSign/, "post", (req) => {
    return {
        "data":{
            appId: "wxc2efec250f2952a3",
            nonceStr: "9FD81843AD7F202F26C1A174C7357585",
            signature: "7bd061487e66a7b4527679263e7bab005d1ad8eb",
            timestamp: "1591587797"
        }
    };
});

Mock.mock(/\/api\/Server\/GetProvinceCity/, "post", (req) => {
    return {
        success: true,
        msg: "",
        data: [{
                province: "云南",
                city: "昆明"
            },
            {
                province: "内蒙",
                city: "呼和浩特"
            },
            {
                province: "北京",
                city: "北京"
            },
            {
                province: "吉林",
                city: "长春"
            },
            {
                province: "四川",
                city: "成都"
            },
            {
                province: "天津",
                city: "天津"
            },
            {
                province: "宁夏",
                city: "银川"
            },
            {
                province: "安徽",
                city: "合肥"
            },
            {
                province: "山东",
                city: "济南"
            },
            {
                province: "山东",
                city: "青岛"
            },
            {
                province: "山东",
                city: "烟台"
            },
            {
                province: "广东",
                city: "东莞"
            },
            {
                province: "广东",
                city: "广州"
            },
            {
                province: "广东",
                city: "深圳"
            }
        ]
    }
});

Mock.mock(/\/api\/Server\/GetNearestCounter/, "post", (req) => {
    return {"success":true,"msg":"","data":[{"id":60,"counterName":"上海八佰伴","marsCode":"AD0","province":"上海","city":"上海","address":"上海市张杨路501号","longitude":121.517982,"latitude":31.2282189,"distance":2.6073},{"id":64,"counterName":"上海恒隆港汇","marsCode":"NP0","province":"上海","city":"上海","address":"上海市浦东新区世纪大道8号国金IFC","longitude":121.501858,"latitude":31.236862,"distance":4.3845},{"id":62,"counterName":"上海大丸","marsCode":"G80","province":"上海","city":"上海","address":"上海市南京东路228号","longitude":121.4825035,"latitude":31.2365809,"distance":6.1092},{"id":80,"counterName":"上海市百一店","marsCode":"MD0","province":"上海","city":"上海","address":"上海南京东路830号","longitude":121.4740699,"latitude":31.235987,"distance":6.8671},{"id":61,"counterName":"上海百盛","marsCode":"MZ0","province":"上海","city":"上海","address":"上海市卢湾区淮海中路918号淮海百盛","longitude":121.4594402,"latitude":31.2171452,"distance":8.0709},{"id":66,"counterName":"上海梅陇镇","marsCode":"A40","province":"上海","city":"上海","address":"上海市南京西路1038号","longitude":121.4570979,"latitude":31.229666,"distance":8.3353},{"id":65,"counterName":"上海久光","marsCode":"AT0","province":"上海","city":"上海","address":"上海静安区南京西路1618号","longitude":121.4463069,"latitude":31.223254,"distance":9.3116},{"id":63,"counterName":"上海国金","marsCode":"B15","province":"上海","city":"上海","address":"上海市虹桥路1号港汇恒隆","longitude":121.4372385,"latitude":31.1944205,"distance":10.5993},{"id":41,"counterName":"嘉兴八佰伴","marsCode":"AX5","province":"浙江","city":"嘉兴","address":"嘉兴市南湖区越秀南路968号嘉兴八佰伴","longitude":120.732969,"latitude":30.758165,"distance":92.9927},{"id":54,"counterName":"南通八佰伴","marsCode":"GT0","province":"江苏","city":"南京","address":"南通市人民中路47号南通八佰伴","longitude":120.865435,"latitude":32.016849,"distance":109.4742}]}
});
Mock.mock(/\/api\/Server\/SearchNearestCounter/, "post",(req)=>{
    return {"success":true,"msg":"","data":[{"id":38,"counterName":"呼和浩特振华","marsCode":"B75","province":"内蒙","city":"呼和浩特","address":"呼和浩特市回民区乌兰恰特西街与锡林郭勒北路振华广场","longitude":111.6688488,"latitude":40.8191041,"distance":1388.0387}]}
});
Mock.mock(/\/api\/Server\/GetBaListByCounterCode/,'post',(req)=>{
    return {"success":true,"msg":"","data":[{"createdBy":null,"createdDate":null,"lastModifiedBy":null,"lastModifiedDate":"2020-06-10T11:41:16.000+0800","empId":"AF16S2957","encodeEmpId":"f4ff1ddff3029f0e52eed4e7908fe61a","empName":"孙永芝","empNameEn":null,"alias":"Michelle","qrCode":"https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vc5cd00b2aa034b3da","avatar":"https://wework.qpic.cn/wwhead/nMl9ssowtibVGyrmvBiaibzDgYBkTiciaJ8ccRzd2t2CNhe3xuOj7G2PRXzbqoxibqeYHOEf0micoJVU1g/0","thumbAvatar":"https://wework.qpic.cn/wwhead/nMl9ssowtibVGyrmvBiaibzDgYBkTiciaJ8ccRzd2t2CNhe3xuOj7G2PRXzbqoxibqeYHOEf0micoJVU1g/100","gender":null,"mobile":"18918698900","position":"上海大丸-柜台经理","positionAnother":null,"externalPosition":null,"marsCode":"G80","brandCode":null,"deptId":18,"isDeleted":0,"isSync":1,"phone":null,"email":"","address":"","deptName":"上海大丸","configId":null,"contactQrCode":null,"publicQrCode":null,"ticket":null,"beautyQr":null,"external_profile":null,"employeeOrgRelList":null,"serviceStar":null},{"createdBy":null,"createdDate":null,"lastModifiedBy":null,"lastModifiedDate":"2020-06-10T11:41:16.000+0800","empId":"AF17F3656","encodeEmpId":"a6336421c4da124405463e34bffc9494","empName":"彭娇娇","empNameEn":null,"alias":"","qrCode":"https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vc8f391c6217dccd5a","avatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6hP29pyc2bibFz4wN36c5JecglRJsphERN97u3ibgcQZLFo/0","thumbAvatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6hP29pyc2bibFz4wN36c5JecglRJsphERN97u3ibgcQZLFo/100","gender":null,"mobile":"13918328714","position":"上海大丸-脸庞设计师","positionAnother":null,"externalPosition":null,"marsCode":"G80","brandCode":null,"deptId":18,"isDeleted":0,"isSync":1,"phone":null,"email":"","address":"","deptName":"上海大丸","configId":null,"contactQrCode":null,"publicQrCode":null,"ticket":null,"beautyQr":null,"external_profile":null,"employeeOrgRelList":null,"serviceStar":null},{"createdBy":null,"createdDate":null,"lastModifiedBy":null,"lastModifiedDate":"2020-06-10T11:41:16.000+0800","empId":"AF18CC796","encodeEmpId":"ce0e992a7ab0bd849040bbae903c256d","empName":"张曼曼","empNameEn":null,"alias":"","qrCode":"https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vc595a3aead968b234","avatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6h8WIap3FVBr4x3Sbs5M60fnwHOTtqREZNdlQQaonNTb0/0","thumbAvatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6h8WIap3FVBr4x3Sbs5M60fnwHOTtqREZNdlQQaonNTb0/100","gender":null,"mobile":"15202128347","position":"上海大丸-脸庞设计师","positionAnother":null,"externalPosition":null,"marsCode":"G80","brandCode":null,"deptId":18,"isDeleted":0,"isSync":1,"phone":null,"email":"","address":"","deptName":"上海大丸","configId":null,"contactQrCode":null,"publicQrCode":null,"ticket":null,"beautyQr":null,"external_profile":null,"employeeOrgRelList":null,"serviceStar":null},{"createdBy":null,"createdDate":null,"lastModifiedBy":null,"lastModifiedDate":"2020-06-10T11:41:16.000+0800","empId":"AF18G2942","encodeEmpId":"cb0f85f02c24cef6c58bb0188d21acfb","empName":"邵欣如","empNameEn":null,"alias":"","qrCode":"https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vc6fbebce4efc5f85e","avatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6hFJCLb3g94IQS9wO9aTic9EHrev98gwC7oG2MDGctsgso/0","thumbAvatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6hFJCLb3g94IQS9wO9aTic9EHrev98gwC7oG2MDGctsgso/100","gender":null,"mobile":"13585546763","position":"上海大丸-脸庞设计师","positionAnother":null,"externalPosition":null,"marsCode":"G80","brandCode":null,"deptId":18,"isDeleted":0,"isSync":1,"phone":null,"email":"","address":"","deptName":"上海大丸","configId":null,"contactQrCode":null,"publicQrCode":null,"ticket":null,"beautyQr":null,"external_profile":null,"employeeOrgRelList":null,"serviceStar":null},{"createdBy":null,"createdDate":null,"lastModifiedBy":null,"lastModifiedDate":"2020-06-10T11:41:16.000+0800","empId":"AF19AX902","encodeEmpId":"dc89f05a365c904af3c20144736fe27c","empName":"陈迦南","empNameEn":null,"alias":"","qrCode":"https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vcdc1f446b81adddca","avatar":"https://wework.qpic.cn/wwhead/nMl9ssowtibVGyrmvBiaibzDgYBkTiciaJ8ccQ91EjsMC7Qn0JZFgJJ0epVnZjgHDOLyomicicNVP7gPRo/0","thumbAvatar":"https://wework.qpic.cn/wwhead/nMl9ssowtibVGyrmvBiaibzDgYBkTiciaJ8ccQ91EjsMC7Qn0JZFgJJ0epVnZjgHDOLyomicicNVP7gPRo/100","gender":null,"mobile":"18721349240","position":"上海大丸-脸庞设计师","positionAnother":null,"externalPosition":null,"marsCode":"G80","brandCode":null,"deptId":18,"isDeleted":0,"isSync":1,"phone":null,"email":"","address":"","deptName":"上海大丸","configId":null,"contactQrCode":null,"publicQrCode":null,"ticket":null,"beautyQr":null,"external_profile":null,"employeeOrgRelList":null,"serviceStar":null},{"createdBy":null,"createdDate":null,"lastModifiedBy":null,"lastModifiedDate":"2020-06-10T11:41:16.000+0800","empId":"AF19CZ289","encodeEmpId":"1a58c68c4db8a61f27cc7458f83e5807","empName":"姚亚楠","empNameEn":null,"alias":"","qrCode":"https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vc6efc6be619ddbf6d","avatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6hqtsQxlMA3In3Z1xiazjydmWaFNBb053ao1lPM5DP1JBw/0","thumbAvatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6hqtsQxlMA3In3Z1xiazjydmWaFNBb053ao1lPM5DP1JBw/100","gender":null,"mobile":"18516167068","position":"上海大丸-脸庞设计师","positionAnother":null,"externalPosition":null,"marsCode":"G80","brandCode":null,"deptId":18,"isDeleted":0,"isSync":1,"phone":null,"email":"","address":"","deptName":"上海大丸","configId":null,"contactQrCode":null,"publicQrCode":null,"ticket":null,"beautyQr":null,"external_profile":null,"employeeOrgRelList":null,"serviceStar":null},{"createdBy":null,"createdDate":null,"lastModifiedBy":null,"lastModifiedDate":"2020-06-10T11:41:16.000+0800","empId":"AF20C8532","encodeEmpId":"00a5b85043d5c4086d1334df19cc79db","empName":"刘金杰","empNameEn":null,"alias":"","qrCode":"https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vc9eb47cb3491b6c56","avatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6hdhllcASMDcJvYib5wFA339xlic3OAALaDJ3sDicypUo4W8/0","thumbAvatar":"https://wework.qpic.cn/wwhead/duc2TvpEgST9hicuyypLEKME09sZXcU6hdhllcASMDcJvYib5wFA339xlic3OAALaDJ3sDicypUo4W8/100","gender":null,"mobile":"15021057080","position":"上海大丸-脸庞设计师","positionAnother":null,"externalPosition":null,"marsCode":"G80","brandCode":null,"deptId":18,"isDeleted":0,"isSync":1,"phone":null,"email":"","address":"","deptName":"上海大丸","configId":null,"contactQrCode":null,"publicQrCode":null,"ticket":null,"beautyQr":null,"external_profile":null,"employeeOrgRelList":null,"serviceStar":true}]}
});