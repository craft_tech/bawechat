let API_URL = "";

if (process.env.NODE_ENV === "production") {
    if (location.href.indexOf("uat") == -1 && location.href.indexOf("10.99") == -1) {
        API_URL = process.env.VUE_APP_LRP_API_prod
    } else {
        API_URL = process.env.VUE_APP_LRP_API_test
    }
} else {
    API_URL = ''
}
export default API_URL;