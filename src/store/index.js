import Vue from 'vue'
import Vuex from 'vuex'
import $ from "jquery";
import {
    Toast
} from "vant";
import {
    browser,
    getChangeTime,
    getQueryParameter
} from "@/libs/utils.js";

Vue.use(Vuex)

import API_URL from '@/config.js'

export default new Vuex.Store({
    state: {
        token1: '',
        token2: '',
        timestamp: '',
        isLoading: false,
        location: {
            latitude: "",
            longitude: ""
        },
        baList: [],
        baImg: '',
        isResultTipsShow: false, // 已绑定ba
        userInfo: {
            openid: process.env.NODE_ENV === "production"?'':'1',
            unionid: '',
        },
        cityList: [],
        shopList: [],
    },
    getters: {
        baList(state) {
            if (state.baList.length == 0) return [];
            let needTopItemArr = [];
            for (let i = 0; i < state.baList.length; i++) {
                if (state.baList[i].serviceStar) {
                    needTopItemArr.push(state.baList[i]);
                    state.baList.splice(i, 1);
                    i--;
                }
            }
            return needTopItemArr.concat(state.baList)
        }
    },
    mutations: {
        setIsLoading(state, params) {
            state.isLoading = params;
        },
        setToken1(state, params) {
            state.token1 = params;
        },
        setToken2(state, params) {
            state.token2 = params;
        },
        setTimeStamp(state, params) {
            state.timestamp = params;
        },
        setUserInfo(state, params) {
            state.userInfo = params;
        },
        set_location(state, params) {
            state.location = params;
        },
        set_isResultTipsShow(state, params) {
            state.isResultTipsShow = params;
        },
        set_baList(state, params) {
            state.baList = params;
        },
        set_baImg(state, params) {
            state.baImg = params;
        },
        set_cityList(state, params) {
            state.cityList = params;
        },
        set_shopList(state, params) {
            state.shopList = params;
        },
    },
    actions: {
        oauth({
            commit
        }) {
            return new Promise((resolve, reject) => {
                let token = getQueryParameter('token')
                if (token) {
                    commit("setToken1", token);
                    resolve()
                } else {
                    location.href = API_URL + '/api/Server/Oauth?siteurl=' + encodeURIComponent(location.href);
                }
            })
        },
        getInfo({
            state,
            commit
        }) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: API_URL + "/api/Server/GetInfo",
                    type: "post",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        token: state.token1
                    }),
                    beforeSend: function() {
                        commit('setIsLoading', true);
                    },
                    complete: function() {
                        commit('setIsLoading', false);
                    },
                    success: function(data) {
                        console.log(data)
                        commit("setUserInfo", data.data.data);
                        commit("setToken2", data.data.token);
                        // commit("setTimeStamp", data.timestamp)
                        if (data.data.png) {
                            commit("set_baImg", data.data.png);
                            commit("set_isResultTipsShow", true);
                        }
                        resolve(true)
                    },
                    error: function(msg) {
                        console.log("err", msg);
                        // 上线时切换
                        setTimeout(() => {
                            // location.href = API_URL + '/api/Server/Oauth?siteurl=' + encodeURIComponent(location.href);
                        }, 500)
                        reject(msg)
                    },
                });
            })
        },
        wxsdk({
            state
        }, obj) {
            return new Promise((resolve, reject) => {
                var formobj = {};
                for (var key in obj.openData) {
                    formobj[key] = obj.openData[key];
                }
                var xhr = new XMLHttpRequest();
                xhr.open(obj.openType, obj.url, true);
                xhr.setRequestHeader(
                    "Authorization",
                    "Bearer " + state.token2
                );
                xhr.setRequestHeader("content-type", "application/json;charset=utf-8");
                xhr.responseType = "json";
                xhr.onload = function() {
                    console.log(obj, this.status)
                    try {
                        if (this.status == 200) {
                            var config = this.response.data;
                            console.log("config", config);

                            var wxConfig = {
                                debug: false,
                                beta: true,
                                appId: config.appId,
                                timestamp: config.timestamp * 1,
                                nonceStr: config.nonceStr,
                                signature: config.signature,
                                jsApiList: ["getLocation"],
                                // "addCard",
                                // "onMenuShareTimeline",
                                // "onMenuShareAppMessage",
                                // "onMenuShareQQ",
                                // "onMenuShareWeibo",
                            };
                            wx.config(wxConfig);
                        }
                    } catch (error) {
                        console.log(error)
                        resolve(true);
                    }
                };
                xhr.send(JSON.stringify(formobj));

                //绑定微信分享事件
                try {
                    wx.ready(function() {
                        console.log('wx ready')
                        resolve(true);
                    });
                } catch (e) {
                    var msg = "错误：微信js-sdk未引用或者错误!";
                    try {
                        console.log(msg);
                    } catch (e) {
                        alert(msg);
                    }
                    return;
                }
            })
        },
        sdkGetLocation({
            commit
        }) {
            return new Promise((resolve, reject) => {
                var location = {
                    latitude: "",
                    longitude: ""
                };
                try {
                    console.log('wxsdk')
                    wx.getLocation({
                        type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
                        success: function(res) {
                            var latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
                            var longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
                            var speed = res.speed; // 速度，以米/每秒计
                            var accuracy = res.accuracy; // 位置精度
                            console.log('wxsdk location', res)

                            location.latitude = res.latitude
                            location.longitude = res.longitude

                            commit('set_location', location)
                            resolve(location)
                        },
                        fail: function(err) {
                            console.log('sdkerr', err)
                            resolve(location)
                        },
                        complete: function() {
                            console.log('sdk complete')
                            resolve(location)
                        }
                    });
                } catch (e) {
                    var msg = '错误：微信js-sdk未引用或者错误!';
                    console.log(msg, e)
                }
            })
        },
        // 获取省市
        getArea({
            state,
            commit
        }, obj) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: API_URL + "/api/Server/GetProvinceCity",
                    type: "post",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    headers: {
                        Authorization: 'Bearer ' + state.token2
                    },
                    beforeSend: function() {
                        commit('setIsLoading', true);
                    },
                    complete: function() {
                        commit('setIsLoading', false);
                    },
                    success: function(data) {
                        commit("set_cityList", data.data);
                        resolve(true)
                    },
                    error: function(msg) {
                        console.log("err", msg);
                        reject(msg)
                    },
                });
            })
        },
        getNearShop({
            state,
            commit
        }, obj) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: API_URL + "/api/Server/GetNearestCounter",
                    type: "post",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    headers: {
                        Authorization: 'Bearer ' + state.token2
                    },
                    data: JSON.stringify({
                        longitude: state.location.longitude + '',
                        latitude: state.location.latitude + ''
                    }),
                    beforeSend: function() {
                        commit('setIsLoading', true);
                    },
                    complete: function() {
                        commit('setIsLoading', false);
                    },
                    success: function(data) {
                        commit("set_shopList", data.data);
                        resolve(true)
                    },
                    error: function(msg) {
                        console.log("err", msg);
                        reject(msg)
                    },
                });
            })
        },
        // 获取门店
        getShop({
            state,
            commit
        }, obj) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: API_URL + "/api/Server/SearchNearestCounter",
                    type: "post",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    headers: {
                        Authorization: 'Bearer ' + state.token2
                    },
                    data: JSON.stringify({
                        longitude: state.location.longitude + '',
                        latitude: state.location.latitude + '',
                        province: obj.province,
                        city: obj.city,
                    }),
                    beforeSend: function() {
                        commit('setIsLoading', true);
                    },
                    complete: function() {
                        commit('setIsLoading', false);
                    },
                    success: function(data) {
                        commit('set_shopList', data.data)
                        resolve(true)
                    },
                    error: function(msg) {
                        console.log("err", msg);
                        reject(msg)
                    },
                });
            })
        },
        getBaList({
            state,
            commit
        }, obj) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: API_URL + "/api/Server/GetBaListByCounterCode",
                    type: "post",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    headers: {
                        Authorization: 'Bearer ' + state.token2
                    },
                    data: JSON.stringify({
                        info: state.userInfo.openid,
                        timestamp: state.timestamp,
                        token: state.token2,
                        counterCode: obj.code,
                    }),
                    beforeSend: function() {
                        commit('setIsLoading', true);
                    },
                    complete: function() {
                        commit('setIsLoading', false);
                    },
                    success: function(data) {
                        commit("set_baList", data.data);
                        resolve(true)
                    },
                    error: function(msg) {
                        console.log("err", msg);
                        reject(msg)
                    },
                });
            })
        },
        getBAInfo({
            state,
            commit
        }, obj) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: API_URL + "/api/Server/GetBaCardByEmployeeCode",
                    type: "post",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    headers: {
                        Authorization: 'Bearer ' + state.token2
                    },
                    data: JSON.stringify({
                        info: state.userInfo.openid,
                        timestamp: state.timestamp,
                        token: state.token2,
                        employeeCode: obj.employeeCode,
                    }),
                    beforeSend: function() {
                        commit('setIsLoading', true);
                    },
                    complete: function() {
                        // commit('setIsLoading', false);
                    },
                    success: function(data) {
                        commit("set_baImg", data.data);
                        let img = new Image();
                        img.onload = function() {
                            commit('setIsLoading', false);
                            resolve(true)
                        }
                        img.src = data.data;
                    },
                    error: function(msg) {
                        console.log("err", msg);
                        reject(msg)
                    },
                });
            })
        },
        TackingLog({
            state,
            commit
        }, obj) {
            $.ajax({
                url: API_URL + "/api/Server/TackingLog",
                type: "post",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                headers: {
                    Authorization: 'Bearer ' + state.token2
                },
                data: JSON.stringify({
                    info: state.userInfo.openid,
                    timestamp: state.timestamp,
                    token: state.token2,
                    employeeCode: obj.employeeCode,
                    baName: obj.baName
                }),
                success: function(data) {
                    console.log(data)
                },
                error: function(msg) {
                    console.log("err", msg);
                },
            });
        }
    },
    modules: {}
})