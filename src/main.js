import Vue from "vue";
import "@/libs/rem";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import utils from "./libs/utils";

import vant from "@/libs/vant";
Vue.use(vant);
Vue.prototype._Utils = utils;
Vue.config.productionTip = false;

// 授权拿到token后贴入后缀测试
// import "@/mock";
// import VConsole from "vconsole";
// const vConsole = new VConsole();

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
